class PostsController < ApplicationController
    def new
        @post = Post.new
    end

    def create
        @post = Post.new(post_params)
        if @post.save
          flash[:success] = "Post successfully created"
          redirect_to @post
        else
          flash[:error] = "Something went wrong"
          render 'new'
        end
    end
    
    def index
        @posts = Post.all
    end

    def edit
        @post = Post.find(params[:id])
    end

    def update
        @post = Post.find(params[:id])
        if @post.update(post_params)
          flash[:success] = "Object was successfully updated"
          redirect_to @post
        else
          flash[:error] = "Something went wrong"
          render 'edit'
        end
    end

    def show
        @post = Post.find(params[:id])
    end

    def destroy
        @post = Post.find(params[:id])
        if @post.destroy
            flash[:success] = 'Object was successfully deleted.'
            redirect_to posts_path
        else
            flash[:error] = 'Something went wrong'
            redirect_to posts_path
        end
    end
    

    private
        def post_params
            params.require(:post).permit(:title, :content)
        end
    
    
end
